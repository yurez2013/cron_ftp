<?php
/**
 * Конфіги підключення до бази даних
 */
DB::$dbName = 'cron_ftp';
DB::$user = 'root';
DB::$password = '';
DB::$host = 'localhost';
DB::$port = null;
DB::$encoding = 'utf-8';

/**
 * Url - до до корення проекту який буде заливатись на фтп
 */
define("URL_DERECTORY", "/OpenServer/domains/testapi");

/**
 * Конфіги підключення до фтп
 */
define("FTP_HOST", "host");
define("FTP_PORT", "");
define("FTP_USERNAME", "username");
define("FTP_PASSWORD", "pass");