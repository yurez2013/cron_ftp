<?php

class ftp
{
    public $url_from_derectory = '';
    
    public function __construct($url)
    {
        $this->url_from_derectory = $url;
    }

    public function connectFtp()
    {
        $open = ftp_connect(FTP_HOST, 21, 30);
        if (!ftp_login($open, FTP_USERNAME, FTP_PASSWORD)) exit("Не могу соединиться");
        return $open;
    }

    public function deleteFolderFtp($patch)
    {
        $parts = explode('/', $patch);
        $delete = array();
        foreach ($parts as $part) {
            $delete[] = stristr($patch, $part, true);
        }
        krsort($delete);
        foreach ($delete as $pat) {
            ftp_rmdir($this->connectFtp(), $pat);
        }


    }

    public function pushToFtp($url_local_file, $folderTree, $file)
    {
        $folderTree = $folderTree.'ftp';
        $parts = explode('/', $folderTree);

        foreach ($parts as $part) {

            if(!empty($part)){
                if (!@ftp_mkdir($this->connectFtp(), '')) {
                    ftp_mkdir($this->connectFtp(), stristr($folderTree, $part, true));
                }
            }

        }

        ftp_put($this->connectFtp(), $url_local_file, $this->url_from_derectory.$url_local_file, FTP_BINARY);

    }

    public function getPatchFromFolder()
    {
        $data = DB::queryFirstColumn("SELECT patch FROM data_ftp WHERE actual = 1");
        return $data;
    }


    public function getData($where)
    {
        $data = DB::queryFirstRow("SELECT * FROM data_ftp WHERE patch=%s AND actual = 1", $where);
        return $data;
    }
    
    public function deleteFileAndFolder($id)
    {
        DB::delete('data_ftp', "id=%s", $id);
    }
    
    public function updateFileAndFolder($patch)
    {
        DB::update('data_ftp', array(
            'actual' => '0'
        ), "patch=%s", $patch);

    }

    public function getFileInDirectory($dir = '')
    {
        $files = array();
        if ($handle = opendir($dir)) {
            while (false !== ($item = readdir($handle))) {
                if (is_file("$dir/$item")) {
                    $files[] = "$dir/$item";
                } elseif (is_dir("$dir/$item") && ($item != ".") && ($item != "..")) {
                    $files = array_merge($files, $this->getFileInDirectory("$dir/$item"));
                }
            }
            closedir($handle);
        }
        return $files;
    }


    public function insertFileAndFolder($params = array())
    {
        try {
            DB::insert('data_ftp', $params);
        } catch (MeekroDBException $e) {
            return false;
        }

    }

    public function saveAndPushFileAndFolderToFtp()
    {

        foreach ($this->getFileInDirectory($this->url_from_derectory) as $fileAndFolder) {

            if(!in_array(str_replace($this->url_from_derectory, '', $fileAndFolder), $this->getPatchFromFolder())) {
                if (is_file($fileAndFolder)) {
                    $this->insertFileAndFolder(array(
                        'hash' => hexdec(crc32(str_replace($this->url_from_derectory, '', $fileAndFolder))),
                        'patch' => str_replace($this->url_from_derectory, '', $fileAndFolder),
                        'type' => 'file',
                        'size' => filesize($fileAndFolder),
                        'last_scan' => date("Y:m:d h:m:s"),
                        'actual' => true,
                    ));
                    $file = basename($fileAndFolder);

                    $folderTree = str_replace($file, '', str_replace($this->url_from_derectory, '', $fileAndFolder));
                    $this->pushToFtp(str_replace($this->url_from_derectory, '', $fileAndFolder), $folderTree, $file);
                }
            } else {
                $data = $this->getData(str_replace($this->url_from_derectory, '', $fileAndFolder));

                if($data['size'] != filesize($fileAndFolder)) {
                    $this->deleteFileAndFolder($data['id']);

                    $this->insertFileAndFolder(array(
                        'hash' => hexdec(crc32(str_replace($this->url_from_derectory, '', $fileAndFolder))),
                        'patch' => str_replace($this->url_from_derectory, '', $fileAndFolder),
                        'type' => 'file',
                        'size' => filesize($fileAndFolder),
                        'last_scan' => date("Y:m:d h:m:s"),
                        'actual' => true,
                    ));
                    $file = basename($fileAndFolder);

                    $folderTree = str_replace($file, '', str_replace($this->url_from_derectory, '', $fileAndFolder));
                    $this->pushToFtp(str_replace($this->url_from_derectory, '', $fileAndFolder), $folderTree, $file);
                }
            }
        }


        foreach ($this->getPatchFromFolder() as $patch) {
            if(!in_array($this->url_from_derectory.$patch, $this->getFileInDirectory($this->url_from_derectory))) {
                $this->updateFileAndFolder($patch);
                ftp_delete($this->connectFtp(), $patch);
                $this->deleteFolderFtp($patch);
            }
        }

    }

}